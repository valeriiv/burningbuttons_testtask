# burningbuttons-testtask
Testtask for BurningButtons

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

The API resource - https://swapi.co/documentation
What your app should doing: 
1. Show the results from the API call for any resource (please implement as many resources as you can). 
2. Each resource should be shown on it's own tab (please use a router for that and save a tab name in the url). 
3. Implement the ability of search by name and pagination. 
4. User has an ability to make some note for any resource's item, it should be look like tooltip under the item. All the notes should be restored after browser page reloading. 
5. Make a language toggle between English and Wookiee*

CODE REVIEW from 06/05/2019
очень плохо читается код:
- нет коментарив
- зачем то используются побитовые операции
- очень плохо организован код в компанентах
- непонятно, зачем прописывается computed свойства для data
- запросы написаны прямо в компонентах и обрабатываются прямо там же - очень сложно читать компонент
- зачем то используется отдельный запрос для определения количества записей
- поиск по записям реализован только для тех записей, которые были скачаны для конкретной страницы
- огромные try/catch блоки, в которые помещаются методы полностью