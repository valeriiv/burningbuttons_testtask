import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: "history",
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/films',
      name: 'filmsViewer',
      component: () => import('./views/FilmsViewer.vue')
    },
    {
      path: '/people',
      name: 'peopleViewer',
      component: () => import('./views/PeopleViewer.vue')
    },
    {
      path: '/planets',
      name: 'planetsViewer',
      component: () => import('./views/PlanetsViewer.vue')
    },
    {
      path: '/species',
      name: 'speciesViewer',
      component: () => import('./views/SpeciesViewer.vue')
    },
    {
      path: '/starships',
      name: 'starshipsViewer',
      component: () => import('./views/StarshipsViewer.vue')
    },
    {
      path: '/vehicles',
      name: 'vehiclesViewer',
      component: () => import('./views/VehiclesViewer.vue')
    }
  ]
})
