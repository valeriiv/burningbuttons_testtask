module.exports = {
    devServer: {
        compress: true,
        hot: true,
        port: 9013,
        open: true
    }
};